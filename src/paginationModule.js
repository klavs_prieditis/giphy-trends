export const paginationModuleKey = "pagination"

const PAGE_SIZE = 25

const select = state => state[paginationModuleKey]

export const selectCurrentPage = state => select(state).currentPage
export const selectLastPage = state => select(state).lastPage
export const selectIsLoading = state => select(state).isLoading
export const selectResponseByPage = (state, page) => select(state).responseByPage[page]
export const selectPageImages = (state) => {
    const response = selectResponseByPage(state, selectCurrentPage(state))

    if (!response || !response.data) return undefined

    return response.data.map(
        d => ({
            id: d.id,
            title: d.title,
            url: d.images["fixed_width"].url,
            width: parseInt(d.images["fixed_width"].width, 10),
            height: parseInt(d.images["fixed_width"].height, 10),
            size: parseInt(d.images["fixed_width"].size, 10)
        })
    )
}
export const selectPageImageSize = (state) => {
    var images = selectPageImages(state)

    if (images === undefined) return undefined

    return selectPageImages(state).reduce(
        (acc, curr) => {
            return acc + curr.size
        },
        0
    )
}

export const CHANGE_PAGE_INIT = "CHANGE_PAGE_INIT"
export const CHANGE_PAGE_DONE = "CHANGE_PAGE_DONE"
export const CHANGE_PAGE_FAIL = "CHANGE_PAGE_FAIL"
export const CACHE_RESPONSE = "CACHE_RESPONSE"

export const changePageInit = page => ({
    type: CHANGE_PAGE_INIT,
    page
})

export const changePageDone = page => ({
    type: CHANGE_PAGE_DONE,
    page
})

export const cacheResponse = (page, response) => ({
    type: CACHE_RESPONSE,
    page,
    response
})

export const refreshCurrentPage = () => (dispatch, getState) => {
    const currentPage = selectCurrentPage(getState())
    dispatch(changePage(currentPage))
}

export const changePage = page => changePageInternal(page, fetch)

export const changePageInternal = (page, fetch) => async (dispatch, getState) => {
    dispatch(changePageInit(page))
    try {
        if (selectResponseByPage(getState(), page) === undefined){
            const response = await fetch(`http://api.giphy.com/v1/gifs/trending?api_key=dc6zaTOxFJmzC&offset=${page*PAGE_SIZE}`)
            if (response.ok){
                const data = await response.json()
                dispatch(cacheResponse(page, data))
            }
        }
        dispatch(changePageDone(page))
    } catch (e) {
        dispatch(changePageDone(page))
    }
}

export const initialState = {
    currentPage: 0,
    lastPage: Infinity,
    isLoading: false,
    responseByPage: {}
}

export const paginationReducer = (
    state = initialState,
    action = {}
) => {
    switch (action.type) {
        case CHANGE_PAGE_INIT:
            return ({
                ...state,
                isLoading: true
            })
        case CHANGE_PAGE_DONE:
            return ({
                ...state,
                currentPage: action.page,
                isLoading: false
            })
        case CACHE_RESPONSE:
            return ({
                ...state,
                responseByPage: {
                    ...state.responseByPage,
                    [action.page]: action.response
                },
                isLoading: false
            })
        default:
            return state;
    }
}