import React from "react"
import ReactDOM from "react-dom"
import { shallow } from "enzyme"
import { App } from "./App"

const images =
    [
        {
            id: "26xBFJL7CsZ5ZiAmY",
            url: "https://media0.giphy.com/media/26xBFJL7CsZ5ZiAmY/200w.gif?cid=e1bb72ff5acba9d6743575457328e526",
            width: 200,
            height: 113,
            size: 522367,
            title: "hopping dog show GIF by Westminster Kennel Club"
        },
        {
            id: "d3mnngyvwMQbMJj2",
            url: "https://media0.giphy.com/media/d3mnngyvwMQbMJj2/200w.gif?cid=e1bb72ff5acba9d6743575457328e526",
            width: 200,
            height: 133,
            size: 1104439,
            title: "Sports bar GIF by Originals"
        }
    ]
const imageSize = images.reduce((acc, curr) => acc + curr.size, 0)


it('renders successfully a loaded page', () => {
    const app = shallow(
        <App
            currentPage={0}
            lastPage={Infinity}
            changePage={jest.fn()}
            isLoading={false}
            images={images}
            imageSize={imageSize}
            refreshPage={jest.fn()} />
    )
    expect(app).toMatchSnapshot()
});

it('renders loading page', () => {
    const app = shallow(
        <App
            currentPage={0}
            lastPage={Infinity}
            changePage={jest.fn()}
            isLoading={true}
            images={images}
            imageSize={imageSize}
            refreshPage={jest.fn()} />
    )
    expect(app).toMatchSnapshot()
});
