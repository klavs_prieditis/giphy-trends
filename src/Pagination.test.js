import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { Pagination } from './Pagination.js';

it('renders the first page', () => {
    const app = shallow(
        <Pagination
            currentPage={0}
            lastPage={3}
            onChange={jest.fn()}
        />
    )
    expect(app).toMatchSnapshot()
});

it('renders a middle page', () => {
    const app = shallow(
        <Pagination
            currentPage={1}
            lastPage={3}
            onChange={jest.fn()}
        />
    )
    expect(app).toMatchSnapshot()
});

it('renders the last page', () => {
    const app = shallow(
        <Pagination
            currentPage={3}
            lastPage={3}
            onChange={jest.fn()}
        />
    )
    expect(app).toMatchSnapshot()
});

it('renders with class name', () => {
    const app = shallow(
        <Pagination
            className="MyPagination"
            currentPage={3}
            lastPage={3}
            onChange={jest.fn()}
        />
    )
    expect(app).toMatchSnapshot()
});

it('switches to prev page', () => {
    const onChange = jest.fn();
    const app = shallow(
        <Pagination
            currentPage={1}
            lastPage={2}
            onChange={onChange}
        />
    )
    app.find(".Pagination__control--prev").simulate("click")
    expect(onChange).toBeCalledWith(0)
});

it('switches to next page', () => {
    const onChange = jest.fn();
    const app = shallow(
        <Pagination
            currentPage={1}
            lastPage={2}
            onChange={onChange}
        />
    )
    app.find(".Pagination__control--next").simulate("click")
    expect(onChange).toBeCalledWith(2)
});