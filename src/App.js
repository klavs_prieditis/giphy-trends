import React, { PureComponent } from 'react'
import PropTypes from "prop-types"
import { connect } from 'react-redux'
import { Pagination } from './Pagination'
import { Results } from './Results'
import { selectCurrentPage, selectLastPage, selectIsLoading, selectPageImages, selectPageImageSize, changePage, refreshCurrentPage} from "./paginationModule"
import classNames from "classnames"

import './App.css'

export class App extends PureComponent {
  static propTypes = {
    currentPage: PropTypes.number.isRequired,
    lastPage: PropTypes.number.isRequired,
    changePage: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    images: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired
      })
    ),
    imageSize: PropTypes.number,
    refreshPage: PropTypes.func.isRequired
  }
  componentDidMount() {
    this.props.refreshPage()
  }
  render() {
    const {
      currentPage,
      lastPage,
      changePage,
      isLoading,
      images,
      imageSize,
      refreshPage
    } = this.props

    return (
      <div className={classNames("App", { "App--loading": isLoading })}>
        <Pagination
          className="App__pagination"
          currentPage={currentPage}
          lastPage={lastPage}
          onChange={changePage}
        />
        {
          isLoading ?
            null
            :
            (
              <Results
                className="App__results"
                images={images}
                size={imageSize}
                onRefresh={refreshPage}
              />
            )
        }
      </div>
    )
  }
}

export const AppContainer = connect(
  state => ({
    currentPage: selectCurrentPage(state),
    lastPage: selectLastPage(state),
    isLoading: selectIsLoading(state),
    images: selectPageImages(state),
    imageSize: selectPageImageSize(state),
  }),
  {
    changePage,
    refreshPage: refreshCurrentPage
  }
)(App)