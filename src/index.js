import React from 'react'
import ReactDOM from 'react-dom'
import {
  createStore,
  combineReducers,
  compose,
  applyMiddleware,
} from 'redux'
import {
  Provider
} from 'react-redux'
import thunk from 'redux-thunk'
import registerServiceWorker from './registerServiceWorker'
import {
  AppContainer
} from './App'
import './index.css'
import {
  paginationModuleKey,
  paginationReducer
} from './paginationModule'


const appliedMiddlewares = [thunk];
const createStoreWithMiddleware = compose(
  applyMiddleware(...appliedMiddlewares),
  window.devToolsExtension ? window.devToolsExtension() : (f) => f,
)(createStore);
const reducer = combineReducers({
  [paginationModuleKey]: paginationReducer
});
const store = createStoreWithMiddleware(reducer);


ReactDOM.render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
