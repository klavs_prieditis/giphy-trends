import React, { PureComponent } from 'react';
import PropTypes from "prop-types";
import classNames from "classnames"

import "./Pagination.css";

export class Pagination extends PureComponent {
    static propTypes = {
        className: PropTypes.string,
        currentPage: PropTypes.number.isRequired,
        lastPage: PropTypes.number.isRequired,
        onChange: PropTypes.func.isRequired
    }
    constructor(props) {
        super(props)
        this.onPrev = this.onPrev.bind(this)
        this.onNext = this.onNext.bind(this)
    }
    onPrev() {
        this.props.onChange(this.props.currentPage - 1)
    }
    onNext() {
        this.props.onChange(this.props.currentPage + 1)
    }
    render() {
        const {
            className,
            currentPage,
            lastPage
        } = this.props

        return (
            <div className={classNames("Pagination", className)}>
                {
                    currentPage === 0 ?
                        null
                        :
                        (
                            <button
                                type="button"
                                className="Pagination__control Pagination__control--prev"
                                title="Previous page"
                                aria-label="Previous page"
                                onClick={this.onPrev}
                            >
                                {currentPage - 1}
                            </button>
                        )
                }
                <div className="Pagination__currect-page">Current page: {currentPage}</div>
                {
                    currentPage === lastPage ?
                        null
                        :
                        (
                            <button
                                type="button"
                                className="Pagination__control Pagination__control--next"
                                title="Next page"
                                aria-label="Next page"
                                onClick={this.onNext}
                            >
                                {currentPage + 1}
                            </button>
                        )
                }
            </div>
        )
    }
}