import React from "react"
import ReactDOM from "react-dom"
import { shallow } from "enzyme"
import { Results } from "./Results"

const images =
    [
        {
            id: "26xBFJL7CsZ5ZiAmY",
            url: "https://media0.giphy.com/media/26xBFJL7CsZ5ZiAmY/200w.gif?cid=e1bb72ff5acba9d6743575457328e526",
            width: 200,
            height: 113,
            title: "hopping dog show GIF by Westminster Kennel Club"
        },
        {
            id: "d3mnngyvwMQbMJj2",
            url: "https://media0.giphy.com/media/d3mnngyvwMQbMJj2/200w.gif?cid=e1bb72ff5acba9d6743575457328e526",
            width: 200,
            height: 133,
            title: "Sports bar GIF by Originals"
        }
    ]

it('renders the error dialog', () => {
    const app = shallow(
        <Results onRefresh={jest.fn()} />
    )
    expect(app).toMatchSnapshot()
});

it('handles the error dialog refresh action', () => {
    const onRefresh = jest.fn()
    const app = shallow(
        <Results onRefresh={onRefresh} />
    )
    app.find("button").simulate("click")
    expect(onRefresh).toBeCalled()
});

it('renders images', () => {
    const app = shallow(
        <Results images={images} size={1104439} onRefresh={jest.fn()} />
    )
    expect(app).toMatchSnapshot()
});

it('renders results with class name', () => {
    const app = shallow(
        <Results class="MyResults" images={images} size={1104439} onRefresh={jest.fn()} />
    )
    expect(app).toMatchSnapshot()
});