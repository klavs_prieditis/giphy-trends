import { CHANGE_PAGE, changePageDone, paginationReducer, initialState, CHANGE_PAGE_DONE, changePageInit, CHANGE_PAGE_INIT, CHANGE_PAGE_FAIL, cacheResponse, CACHE_RESPONSE, paginationModuleKey, selectCurrentPage, selectLastPage, selectIsLoading, selectResponseByPage, selectPageImages, selectPageImageSize, changePageInternal } from "./paginationModule.js";

const state = {
    [paginationModuleKey]: {
        currentPage: 0,
        lastPage: Infinity,
        isLoading: false,
        responseByPage: {}
    }
}

const stateWithPageResponse = {
    [paginationModuleKey]: {
        currentPage: 0,
        lastPage: Infinity,
        isLoading: false,
        responseByPage: {
            0: {
                data: [
                    {
                        id: "26xBFJL7CsZ5ZiAmY",
                        images: {
                            fixed_width: {
                                url: "https://media0.giphy.com/media/26xBFJL7CsZ5ZiAmY/200w.gif?cid=e1bb72ff5acba9d6743575457328e526",
                                width: "200",
                                height: "113",
                                size: "522367"
                            }
                        },
                        title: "hopping dog show GIF by Westminster Kennel Club"
                    },
                    {
                        id: "d3mnngyvwMQbMJj2",
                        images: {
                            fixed_width: {
                                url: "https://media0.giphy.com/media/d3mnngyvwMQbMJj2/200w.gif?cid=e1bb72ff5acba9d6743575457328e526",
                                width: "200",
                                height: "133",
                                size: "1104439"
                            }
                        },
                        title: "Sports bar GIF by Originals"
                    }
                ]
            }
        }
    }
}

describe('actions', () => {
    it('creates an action to init page change', () => {
        const page = 2
        const expectedAction = {
            type: CHANGE_PAGE_INIT,
            page
        }
        expect(changePageInit(page)).toEqual(expectedAction)
    })
    it('creates an action to complete page change', () => {
        const page = 2
        const expectedAction = {
            type: CHANGE_PAGE_DONE,
            page
        }
        expect(changePageDone(page)).toEqual(expectedAction)
    })
    it('creates an action to cahce page response', () => {
        const page = 2
        const response = { resp: "mock" }
        const expectedAction = {
            type: CACHE_RESPONSE,
            page,
            response
        }
        expect(cacheResponse(page, response)).toEqual(expectedAction)
    })
})

describe('reducer', () => {
    it('returns the intended initial state', () => {
        expect(paginationReducer()).toEqual(initialState)
    })

    it('handles page change init', () => {
        expect(
            paginationReducer(
                { currentPage: 1 },
                changePageInit(2)
            )
        ).toEqual({
            currentPage: 1,
            isLoading: true
        })
    })

    it('handles page change done', () => {
        expect(
            paginationReducer(
                { currentPage: 1 },
                changePageDone(2)
            )
        ).toEqual({
            currentPage: 2,
            isLoading: false,
            currentPageResponse: undefined
        })
    })

    it('caches page response', () => {
        const page = 2
        const response = { data: "mock" }
        expect(
            paginationReducer(
                { currentPage: 1 },
                cacheResponse(page, response)
            )
        ).toEqual({
            currentPage: 1,
            isLoading: false,
            responseByPage: {
                [page]: response
            }
        })
    })
})


describe('thunks', () => {
    it('handles failed images request', async () => {
        const actions = []
        const changePageThunk = await changePageInternal(
            0,
            () => Promise.reject()
        )(action => actions.push(action), () => state)
        expect(actions).toEqual([
            {
                page: 0,
                type: "CHANGE_PAGE_INIT"
            },
            {
                page: 0,
                type: "CHANGE_PAGE_DONE"
            }
        ])
    })

    it('handles nok status of images request', async () => {
        const actions = []
        const changePageThunk = await changePageInternal(
            0,
            () => Promise.resolve({
                ok: false
            })
        )(action => actions.push(action), () => state)
        expect(actions).toEqual([
            {
                page: 0,
                type: "CHANGE_PAGE_INIT"
            },
            {
                page: 0,
                type: "CHANGE_PAGE_DONE"
            }
        ])
    })

    it('handles failed jsonization of images request', async () => {
        const actions = []
        const changePageThunk = await changePageInternal(
            0,
            () => Promise.resolve({
                ok: true,
                json: () => Promise.reject()
            })
        )(action => actions.push(action), () => state)
        expect(actions).toEqual([
            {
                page: 0,
                type: "CHANGE_PAGE_INIT"
            },
            {
                page: 0,
                type: "CHANGE_PAGE_DONE"
            }
        ])
    })

    it('handles success of images request', async () => {
        const actions = []
        const changePageThunk = await changePageInternal(
            0,
            () => Promise.resolve({
                ok: true,
                json: () => Promise.resolve({ mock: "data" })
            })
        )(action => actions.push(action), () => state)
        expect(actions).toEqual([
            {
                page: 0,
                type: "CHANGE_PAGE_INIT"
            },
            {
                response: { mock: "data" },
                page: 0,
                type: "CACHE_RESPONSE"
            },
            {
                page: 0,
                type: "CHANGE_PAGE_DONE"
            }
        ])
    })
})

describe('selectors', () => {

    it("selects current page", () => {
        expect(selectCurrentPage(state)).toEqual(0)
    })

    it("selects last page", () => {
        expect(selectLastPage(state)).toEqual(Infinity)
    })

    it("selects loading state", () => {
        expect(selectIsLoading(state)).toEqual(false)
    })

    it("selects response by page", () => {
        expect(selectResponseByPage(state, 1)).toEqual(undefined)
    })

    it("selects page images (no response)", () => {
        expect(selectPageImages(state)).toEqual(undefined)
    })

    it("selects page images (has response)", () => {
        expect(selectPageImages(stateWithPageResponse)).toEqual(
            [
                {
                    id: "26xBFJL7CsZ5ZiAmY",
                    url: "https://media0.giphy.com/media/26xBFJL7CsZ5ZiAmY/200w.gif?cid=e1bb72ff5acba9d6743575457328e526",
                    width: 200,
                    height: 113,
                    size: 522367,
                    title: "hopping dog show GIF by Westminster Kennel Club"
                },
                {
                    id: "d3mnngyvwMQbMJj2",
                    url: "https://media0.giphy.com/media/d3mnngyvwMQbMJj2/200w.gif?cid=e1bb72ff5acba9d6743575457328e526",
                    width: 200,
                    height: 133,
                    size: 1104439,
                    title: "Sports bar GIF by Originals"
                }
            ]
        )
    })

    it("selects page image size (no response)", () => {
        expect(selectPageImageSize(state)).toEqual(undefined)
    })

    it("selects page image size (has response)", () => {
        expect(selectPageImageSize(stateWithPageResponse)).toEqual(1626806)
    })
})