import React, { PureComponent } from 'react'
import PropTypes from "prop-types"
import classNames from "classnames"

import "./Results.css"

export class Results extends PureComponent {
    static propTypes = {
        className: PropTypes.string,
        images: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.string.isRequired,
            width: PropTypes.number.isRequired,
            height: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            url: PropTypes.string.isRequired
          })
        ),
        size: PropTypes.number,
        onRefresh: PropTypes.func.isRequired
    }
    render() {
        const {
            className,
            images,
            size,
            onRefresh
        } = this.props
        if (images === undefined) {
            return (
                <div className="Dialog">
                    Whoops... Something went wrong!
                    <button type="button" onClick={onRefresh}>Retry</button>
                </div>
            )
        }
        return (
            <div className={classNames("Results", className)}>
                <div className="Results__info">
                    Size {(size/1024/1024).toFixed(2)}MB
                </div>
                <ol className="Results__list">
                    {
                        images.map(
                            image => (
                                <li key={image.id}>
                                    <img
                                        alt={image.title}
                                        src={image.url}
                                        width={image.width}
                                        height={image.height}
                                    />
                                </li>
                            )
                        )
                    }
                </ol>
            </div>
        )
    }
}